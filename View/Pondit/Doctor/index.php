
<?php
	use App\Pondit\Doctor\Doctor;
	include_once '../../../vendor/autoload.php';
	$obj = new Doctor();
	//print_r($alldata);
$totalItem=$obj->count();
//echo $totalItem;


//App\Bitm\SEIPXXXX\Utility\Utility::dd($allResult);
//Pagination Code
if(array_key_exists('itemPerPage',$_SESSION)){
	if(array_key_exists('itemPerPage',$_GET)) {
		$_SESSION['itemPerPage'] = $_GET['itemPerPage'];
	}
}

else{
	$_SESSION['itemPerPage']=5;
}


$itemPerPage=$_SESSION['itemPerPage'];

//echo $itemPerPage;


$totalPage=ceil($totalItem/$itemPerPage);
//echo $totalPage;
$paginate="";
if(array_key_exists('pageNumber',$_GET)){
	$pageNumber= $_GET['pageNumber'];
}
else{
	$pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
	$class = ( $pageNumber == $i ) ? "active" : "";
	$paginate.="<li class='$class'><a href='index.php?pageNumber=".$i."'>".$i."</a></li>";
}



$pageStartFrom=$itemPerPage*($pageNumber-1);
if(array_key_exists('pageNumber',$_GET)) {
	$prevPage = $_GET['pageNumber'] - 1;
}
if(array_key_exists('pageNumber',$_GET)) {
	$nextPage = $_GET['pageNumber'] + 1;
}

$allResult=$obj->paginator($pageStartFrom,$itemPerPage);


?>

<?php
	if (empty($_GET['order']) or $_GET['order']=='1-9') {
		$_SESSION['order']='1-9';
	}else{
		$_SESSION['order']=$_GET['order'];
	}
	$alldata = $obj->index();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Patient List</title>
</head>
<body>
<div>
	
<a href="create.php"><h3> Doctor Registration </h3></a> 
<a href="trashlist.php"><h3>Patient Searved List</h3></a>
</div>
<a href="index.php"><h2>Patient List</h2></a>
<a href="pdf.php"><h3>PDF Download</h3></a>
<a href="excel.php"><h3>Excel Download</h3></a>
<br>
<a href="index.php?order=a-z">A-Z</a>
<a href="index.php?order=1-9">1-9</a>

<?php
//session_start();
if (isset($_SESSION['Message']) and !empty($_SESSION['Message'])) {
	echo $_SESSION['Message'];
	unset($_SESSION['Message']);
}

?>

<table border="1">
	<th>SL</th>
	
	<th>Doctor Name</th>
	<th>Doctor Degree</th>
	<th>Doctor Speciality</th>
	<th>Employed</th>
	<th colspan="3">Action</th>
	<?php
	$i=0;
	foreach ($alldata as $ondata) {
		$i++;
	?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $ondata['name'] ?></td>
		<td><?php echo $ondata['degree'] ?></td>
		<td><?php echo $ondata['specility'] ?></td>
		<td><?php echo $ondata['employed'] ?></td>
	<td><a href="patientList.php?id=<?php echo $ondata['id'] ?>" >Patient List</a></td>
		<td><a href="edit.php?id=<?php echo $ondata['id'] ?>" >Edit</a></td>
		<td><a href="trash.php?id=<?php echo $ondata['id'] ?>" onclick="return checkDelete()" >Delete</a></td>


	</tr>
	<?php } ?>
</table>
<form action="index.php" method="get">
	<div class="form-group">
		<label for="sel1">Select number of items you want to view</label>
		<select class="form-control" id="sel1" name="itemPerPage">
			<option>5</option>
			<option selected>10</option>
			<option>15</option>
			<option>20</option>
		</select>
		<br>
		<button type="submit" class="btn btn-info">Go!</button>
	</div>
</form>
<h2>All Doctor List</h2>
<ul class="pagination">
	<?php
	if(array_key_exists('pageNumber',$_GET)) {
		if (($_GET['pageNumber']) > 1) {
			echo "<li><a href=index.php?pageNumber=" . $prevPage . ">Previous</a></li>";
		}
	}?>
	<?php echo $paginate?>

	<?php  if(array_key_exists('pageNumber',$_GET)) {
		if (($_GET['pageNumber'])<$totalPage) {
			echo "<li><a href=index.php?pageNumber=" . $nextPage . ">Next</a></li>";
		}
	}?>
<script type="text/javascript">
	
	function checkDelete() {
		return confirm('Are Your Sure to Delete Data..??');
	}
</script>

</body>
</html>