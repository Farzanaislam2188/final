
<?php
	use App\Pondit\Patient\Patient;
	include_once '../../../vendor/autoload.php';
	$obj = new Patient();
	//print_r($alldata);
?>

<?php
	if (empty($_GET['order']) or $_GET['order']=='1-9') {
		$_SESSION['order']='1-9';
	}else{
		$_SESSION['order']=$_GET['order'];
	}
	$alldata = $obj->setData($_SESSION)->index();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Patient List</title>
</head>
<body>
<div>
	
<a href="create.php"><h3> Patient Registration </h3></a> 
<a href="trashlist.php"><h3>Trash List</h3></a>
</div>
<a href="index.php"><h2>Patient List</h2></a>
<a href="pdf.php"><h3>PDF Download</h3></a>
<a href="excel.php"><h3>Excel Download</h3></a>
<br>
<a href="index.php?order=a-z">A-Z</a>
<a href="index.php?order=1-9">1-9</a>

<?php
//session_start();
if (isset($_SESSION['Message']) and !empty($_SESSION['Message'])) {
	echo $_SESSION['Message'];
	unset($_SESSION['Message']);
}

?>

<table border="1">
	<th>SL</th>
	
	<th>Patient Name</th>
	<th>AGE</th>
	<th>Date Of Birth</th>
	<th>Simptom</th>
	<th colspan="3">Action</th>
	<?php
	$i=0;
	foreach ($alldata as $ondata) {
		$i++;
	?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $ondata['name'] ?></td>
		<td><?php echo $ondata['age'] ?></td>
		<td><?php echo $ondata['dob'] ?></td>
		<td><?php echo $ondata['desisses'] ?></td>
		<td><a href="show.php?id=<?php echo $ondata['id'] ?>" >View</a></td>
		<td><a href="edit.php?id=<?php echo $ondata['id'] ?>" >Edit</a></td>
		<td><a href="trash.php?id=<?php echo $ondata['id'] ?>" onclick="return checkDelete()" >Delete</a></td>
		
		
	</tr>
	<?php } ?>
</table>
<script type="text/javascript">
	
	function checkDelete() {
		return confirm('Are Your Sure to Delete Data..??');
	}
</script>

</body>
</html>