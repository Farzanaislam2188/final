<?php
namespace App\Pondit\Patient;
Use PDO;
?>
<?php

/**
* 
*/
class Patient{
	
		public $id ="";
		public $doctor_id;
		public $name ="";
		public $age ="";
		public $dob ="";
		public $desisses ="";
		public $order ="";
		public $dbUser ="root";
		public $pass ="";
		public $conn ="";

	public function __construct()
	{
		session_start();
		$this->conn = new PDO("mysql:host=localhost; dbname=bitmpms", $this->dbUser, $this->pass);
	}

	public function setData($data=''){
	if (array_key_exists('name', $data) && !empty($data['name'])) {
			$this->name = $data['name'];
		}
				if (array_key_exists('age', $data) && !empty($data['age'])) {
			$this->age = $data['age'];
           }
				if (array_key_exists('dob', $data) && !empty($data['dob'])) {
			$this->dob = $data['dob'];
	       }
				if (array_key_exists('desisses', $data) && !empty($data['desisses'])) {
			$this->desisses = $data['desisses'];
	    }
	    if (array_key_exists('id', $data) && !empty($data['id'])) {
			$this->doctor_id= $data['id'];
		}

		if (array_key_exists('order', $data) && !empty($data['order'])) {
			$this->order = $data['order'];
	}
	if (array_key_exists('id', $data) && !empty($data['id'])) {
			$this->id = $data['id'];
	}
	return $this;
}

 public function store(){

	$query = "INSERT INTO `patient` (`id`,`name`,`age`, `dob`, `desisses`, `doctor_id`)VALUES(:id, :name, :age, :dob, :desisses, :doctor_id )";
	$stmt = $this->conn->prepare($query);
	$result = $stmt->execute(array(
		':id'=>null, 
		':name'=>$this->name,
		':age'=>$this->age,
		':dob'=>$this->dob,
		':desisses'=>$this->desisses,
		':doctor_id'=>$this->doctor_id
		));
	if ($result) {
		$_SESSION['Message'] = "<h2>Data succesfully added</h2>";
	}else{
		$_SESSION['Message'] = "<h2>Data not added</h2>";
	}
	header("location:create.php");
}

public function index(){
	if ($this->order == 'a-z') {
		$query= "SELECT * FROM patient  WHERE is_delete = 0 ORDER BY name";
	}else{
		$query= "SELECT * FROM patient WHERE is_delete = 0 ORDER BY id DESC ";
	}
	$stmt = $this->conn->query($query);
	$alldata = $stmt->fetchAll();
	return $alldata ;
}

public function show(){
    $query = "SELECT * FROM `Patient` WHERE `id` = :id";
    $smt = $this->conn->prepare($query);
    $smt->execute(array(':id' => $this->id));
    $rt = $smt->fetch(PDO::FETCH_ASSOC);
    return $rt;

}
public function update(){
	try {
		$query= "UPDATE  SET `Patient`, `name`=:title, `age`= :num,`dob`=:dob,`desisses`=:desisses WHERE id=:id" ;
		$stmt = $this->conn->prepare($query);
		$stmt->execute(array(
			':id'=>$this->id,
			':name'=>$this->title,
			':age'=>$this->num,
			':dob'=>$this->dob,
			':desisses'=>$this->desisses

			));

		if ($stmt) {
		$_SESSION['Message'] = "<h3>Data succesfully UPDATED</h3>";
	}
	header("location:index.php");

	} catch (Exception $e) {
		echo "error".$e->getMessage();
	}
}
	public function delete(){

		$query = "DELETE FROM `Patient` WHERE `id` =:id";
		$stmt  = $this->conn->query($query);
		$stmt->execute();
		if ($stmt) {
			$_SESSION['Message'] = "<h2>Data Succesfully Deleted </h2>";
		}
		header("location:trashlist.php");
	}

	public function trash(){

		try {
			$query= "UPDATE `Patient` SET is_delete= :true WHERE `id`=:id" ;
		$stmt = $this->conn->prepare($query);
		$stmt->execute(
			array(
			':true'=>'1',
			':id' => $this->id)
		);
		
		if ($stmt) {
			$_SESSION['Message'] = "<h2>Data Succesfully Deleted </h2>";
		}
		header("location:index.php");

		} catch (Exception $e) {
		echo "error".$e->getMessage();
	
	}

	}
	public function trashlist(){
		try {
			
			if ($this->order == 'a-z') {
		$query= "SELECT * FROM `Patient`  WHERE is_delete = 1 ORDER BY name";
	}else{
		$query= "SELECT * FROM mobile_models WHERE is_delete = 1 ORDER BY id DESC ";
	}
	$stmt = $this->conn->query($query);
	$alldata = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $alldata ;
		} catch (Exception $e) {
			echo "error".$e->getMessage();
		}

	}
	public function restore(){
		try {
			
			$query = "UPDATE `Patient` SET is_delete=:fals WHERE id = :id";
			$stmt = $this->conn->prepare($query);
			$stmt->execute(array(
				':fals'=>0,
				':id'=>$this->id
				));
			if ($stmt) {
			$_SESSION['Message'] = "<h2>Data Succesfully Restored </h2>";
			header("location:trashlist.php");
		}
		} catch (Exception $e) {
			echo "error".$e->getMessage();
		 }


}

	public function aalldata(){
	
	$query= "SELECT * FROM `Patient`  WHERE is_delete = 0 ORDER BY name";
	$stmt = $this->conn->query($query);
	$alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $alldata;
}

}

?>