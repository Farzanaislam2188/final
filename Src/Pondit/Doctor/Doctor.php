<?php
namespace App\Pondit\Doctor;
Use PDO;
?>
<?php

class Doctor{

		public $id ="";
		public $name ="";
		public $degree ="";
		public $specility ="";
		public $employed ="";
		public $order ="";
		public $dbUser ="root";
		public $pass ="";
		public $conn ="";

	public function __construct()
	{
		session_start();
		$this->conn = new PDO("mysql:host=localhost; dbname=bitmpms", $this->dbUser, $this->pass);
	}

	public function setData($data=''){
	if (array_key_exists('name', $data) && !empty($data['name'])) {
			$this->name = $data['name'];
		}
				if (array_key_exists('degree', $data) && !empty($data['degree'])) {
			$this->degree = $data['degree'];
           }
				if (array_key_exists('specility', $data) && !empty($data['specility'])) {
			$this->specility = $data['specility'];
	       }
				if (array_key_exists('employed', $data) && !empty($data['employed'])) {
			$this->employed = $data['employed'];
	    }

		if (array_key_exists('order', $data) && !empty($data['order'])) {
			$this->order = $data['order'];
	}
	if (array_key_exists('id', $data) && !empty($data['id'])) {
			$this->id = $data['id'];
	}
	return $this;
}

 public function store(){

	$query = "INSERT INTO `doctor` (`id`,`name`,`degree`, `specility`, `employed`)VALUES(:id, :name, :degree, :specility, :employed )";
	$stmt = $this->conn->prepare($query);
	$result = $stmt->execute(array(
		':id'=>null,
		':name'=>$this->name,
		':degree'=>$this->degree,
		':specility'=>$this->specility,
		':employed'=>$this->employed
		));
	if ($result) {
		$_SESSION['Message'] = "<h2>Data succesfully added</h2>";
	}else{
		$_SESSION['Message'] = "<h2>Data not added</h2>";
	}
	header("location:create.php");
}
public function select(){

	$query ="SELECT * FROM doctor ";
	$query = "SELECT * FROM doctor ORDER BY id DESC";
	$stmt = $this->conn->query($query);
	$result =$stmt->fetchAll();
	return $result;

}
public function index(){
	if ($this->order == 'a-z') {
		$query= "SELECT * FROM doctor  WHERE is_delete = 0 ORDER BY title";
	}else{
		$query= "SELECT * FROM doctor WHERE is_delete = 0 ORDER BY id DESC ";
	}
	$stmt = $this->conn->query($query);
	$alldata = $stmt->fetchAll();
	return $alldata ;
}

public function patientList(){
	try {
		$query = "SELECT * FROM patient WHERE doctor_id =". $this->id;
		$stmt = $this->conn->query($query);
		$result =$stmt->fetchAll();
	} catch (Exception $e) {
		echo "error".$e->getMessage();
	}
	return $result;

}
	public function prescription(){
		try {
			$query = "SELECT * FROM patient WHERE doctor_id =". $this->id;
			$stmt = $this->conn->query($query);
			$result =$stmt->fetch();
		} catch (Exception $e) {
			echo "error".$e->getMessage();
		}
		return $result;

	}
	public function count(){
		$query="SELECT COUNT(*) AS totalItem FROM `doctor` WHERE is_delete = 0";
		$stmt = $this->conn->query( $query);
		$alldata = $stmt->fetch(PDO::FETCH_ASSOC);
		return $alldata['totalItem'];

	}

	public function paginator($pageStartFrom=0,$LIMIT){
		$query="SELECT * FROM `doctor` WHERE is_delete = 0 LIMIT ".$pageStartFrom.",".$LIMIT;
		//echo $query;
		$stmt = $this->conn->query($query);
		$alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $alldata;
	}

}

?>

